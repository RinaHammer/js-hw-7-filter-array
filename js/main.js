/*Реалізувати функцію фільтру масиву за вказаним типом даних. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].*/

function filterBy(arr, type) {
  let newArr = arr.filter((item) => typeof item !== type);
  return newArr;
}

// Зараз можна викликати функцію і отримати результат у змінній:
let result1 = filterBy(["hello", "world", 23, "23", null], "string");
let result2 = filterBy(
  [true, "world", 23, , { 1: "ybf" }, "23", null],
  "object"
);
let result3 = filterBy(
  ["hello", "world", 23, "23", null, 56, 29, Infinity, NaN],
  "number"
);

console.log(result1);
console.log(result2);
console.log(result3);

// Варіант, коли треба показати нові масиви у консолі
// function filterBy(arr, type) {
//   let newArr = arr.filter((item) => typeof item !== type);
//   console.log(newArr);
// }
// filterBy(["hello", "world", 23, "23", null], "string");
// filterBy([true, "world", 23, , { 1: "ybf" }, "23", null], "object");
// filterBy(["hello", "world", 23, "23", null, 56, 29, Infinity, NaN], "number");
